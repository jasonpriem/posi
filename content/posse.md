---
title: Posse
subtitle: Who has adopted the POSI principles?
date: "2021-03-09"
publishdate: "2021-03-09"
comments: false
---

These organisations have publicly adopted the POSI principles.

- [Crossref](https://www.crossref.org/blog/crossrefs-board-votes-to-adopt-the-principles-of-open-scholarly-infrastructure)
- [Dryad](https://blog.datadryad.org/2020/12/08/dryads-commitment-to-the-principles-of-open-scholarly-infrastructure/)
- [ROR](https://ror.org/blog/2020-12-16-aligning-ror-with-posi/)
- [JOSS](https://blog.joss.theoj.org/2021/02/JOSS-POSI)

---

More questions? Open a [GitLab issue](https://gitlab.com/crossref/posi/-/issues).
